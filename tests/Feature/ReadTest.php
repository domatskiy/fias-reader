<?php

namespace Domatskiy\FiasReader\Tests\Feature;

use Domatskiy\FiasReader;
use Domatskiy\FiasReader\Tests\TestCase;
use Domatskiy\FiasReader\Data;

class ReadTest extends TestCase
{
    public function testAddress()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_ADDRESS_OBJECTS.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\AddressObjects\Obj
             */
            $this->assertInstanceOf(Data\AddressObjects\Obj::class, $obj);
            $this->assertNotEmpty($obj->getCode());
            $this->assertNotEmpty($obj->getActStatus());
        });
    }

    public function testStat()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_ACTSTAT.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\ActualStatuses\ActualStatus
             */
            $this->assertInstanceOf(FiasReader\Data\ActualStatuses\ActualStatus::class, $obj);
        });
    }

    public function testCenter()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_CENTERST.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\CenterStatuses\CenterStatus
             */
            $this->assertInstanceOf(FiasReader\Data\CenterStatuses\CenterStatus::class, $obj);
            # var_dump($p);
        });
    }

    public function testCurrentSt()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_CURENTST.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\CurrentStatuses\CurrentStatus
             */
            $this->assertInstanceOf(Data\CurrentStatuses\CurrentStatus::class, $obj);
            # var_dump($p);
        });
    }

    public function testRoom()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_ROOM.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\Rooms\Room
             */
            // var_dump($p);
            $this->assertInstanceOf(FiasReader\Data\Rooms\Room::class, $obj);
        });
    }

    public function testRoomType()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_ROOMTYPE.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\RoomTypes\RoomType
             */
            $this->assertInstanceOf(Data\RoomTypes\RoomType::class, $obj);
        });
    }

    public function testStructureStatus()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_STRSTAT.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\StructureStatuses\StructureStatus
             */
            $this->assertInstanceOf(Data\StructureStatuses\StructureStatus::class, $obj);
        });
    }

    public function testStead()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_STEADS.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\Steads\Stead
             */
            $this->assertInstanceOf(Data\Steads\Stead::class, $obj);
        });
    }

    public function testHouse()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_HOUSE.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\Houses\House
             */
            $this->assertInstanceOf(Data\Houses\House::class, $obj);
        });
    }

    public function testEstateStatuse()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_ESTSTAT.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\EstateStatuses\EstateStatus
             */
            $this->assertInstanceOf(Data\EstateStatuses\EstateStatus::class, $obj);
        });
    }

    public function testOperStat()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_OPERSTAT.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\OperationStatuses\OperationStatus
             */
            $this->assertInstanceOf(Data\OperationStatuses\OperationStatus::class, $obj);
        });
    }

    public function testNormativeDocumentType()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_NDOCTYPE.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\NormativeDocumentTypes\NormativeDocumentType
             */
            $this->assertInstanceOf(Data\NormativeDocumentTypes\NormativeDocumentType::class, $obj);
        });
    }

    public function testAddressObjectType()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_SOCRBASE.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\AddressObjectTypes\AddressObjectType
             */
            $this->assertInstanceOf(Data\AddressObjectTypes\AddressObjectType::class, $obj);
        });
    }

    public function testNormDoc()
    {
        $FiasReader = new FiasReader(__DIR__.'/data/AS_NORMDOC.XML');
        $FiasReader->read(function ($obj) {
            /**
             * @var $obj Data\NormativeDocumentes\NormativeDocument
             */
            $this->assertInstanceOf(Data\NormativeDocumentes\NormativeDocument::class, $obj);
        });
    }
}
