<?php

namespace Domatskiy\FiasReader\Tests;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @param $path
     * @param int $status
     *
     * @return HandlerStack
     * @throws \Exception
     */
    protected function getHandler($path, $status = 200)
    {
        $response = $this->getResponseFromFile($path, $status);

        // Create a mock and queue two responses.
        $mock = new MockHandler([
            $response
        ]);

        $handler = HandlerStack::create($mock);

        return $handler;
    }

    /**
     * @param $path
     * @param $status
     *
     * @return \GuzzleHttp\Psr7\Response
     * @throws \Exception
     */
    protected function getResponseFromFile($path, $status)
    {
        if ($status < 1) {
            throw new \Exception('not correct status');
        }

        $stream = file_get_contents($path);
        return new \GuzzleHttp\Psr7\Response($status, [], $stream);
    }

    protected function getResponseFromString($string, $status = 200)
    {
        $stream = fopen('data://text/plain;base64,' . base64_encode($string), 'r');
        $stream = stream_get_contents($stream);

        return new \GuzzleHttp\Psr7\Response($status, [], $stream);
    }
}
