<?php

namespace Domatskiy;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Domatskiy\FiasReader\Data;

class FiasReader
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var Serializer
     */
    protected static $serializer;

    /**
     * FiasReader constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        class_exists(SerializedName::class);
        $this->path = $path;
    }

    /**
     * @param callable|null $itemReadCallback
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function read(?callable $itemReadCallback = null)
    {
        $xmlReader = new \XMLReader();
        $xmlReader->open($this->path);
        $path = [];
        $counter = 0;

        while ($xmlReader->read()) {
            if ($xmlReader->nodeType == \XMLReader::ELEMENT && (int)$xmlReader->depth === 0) {
                $path[] = $xmlReader->name;
                continue;
            }

            if ($xmlReader->nodeType == \XMLReader::END_ELEMENT) {
                array_pop($path);
                continue;
            }

            if ($xmlReader->nodeType == \XMLReader::ELEMENT) {
                $path[] = $xmlReader->name === 'Object' ? 'Obj' : $xmlReader->name;
                $className = '\\Domatskiy\\FiasReader\\Data\\'.implode('\\', $path);
                array_pop($path);
                $counter++;

                $xml = $xmlReader->readOuterXml();
                $obj = $this->convertToObject($xml, $className);

                if ($itemReadCallback) {
                    $res = call_user_func($itemReadCallback, $obj, $xml, $counter);
                    if ($res === false) {
                        break;
                    }
                }
            }
        }

        $xmlReader->close();
    }

    /**
     * @param string $xml
     * @param string $class
     * @return array|object
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    protected function convertToObject(string $xml, string $class)
    {
        if ($class === '') {
            throw new \Exception('not correct class name');
        }

        if (!class_exists($class)) {
            throw new \Exception(sprintf('class %s not exits', $class));
        }

        $Serializer = $this->getSerializer();

        $xml = '<?xml version="1.0" encoding="utf-8"?>'.$xml;

        return $Serializer->deserialize($xml, $class, 'xml', [
            'allow_extra_attributes' => true,
        ]);
    }

    /**
     * @return Serializer
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    protected function getSerializer():Serializer
    {
        if (self::$serializer instanceof Serializer) {
            return self::$serializer;
        }

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        // normalizers
        $normalizers = [
            new DateTimeNormalizer(),
            new ArrayDenormalizer(),
            new ObjectNormalizer(
                $classMetadataFactory,
                $metadataAwareNameConverter,
                new PropertyAccessor(),
                new PhpDocExtractor()
            ),
        ];

        //encoders
        $encoders = [
            new XmlEncoder()
        ];

        self::$serializer = new Serializer($normalizers, $encoders);

        return self::$serializer;
    }
}
