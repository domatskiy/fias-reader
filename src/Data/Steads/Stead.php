<?php

namespace Domatskiy\FiasReader\Data\Steads;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Stead
{
    /**
     * @SerializedName("@STEADGUID");
     * @var string
     */
    protected $guid;

    /**
     * @SerializedName("@NUMBER");
     * @var string
     */
    protected $number;

    /**
     * @SerializedName("@REGIONCODE");
     * @var string
     */
    protected $regionCode;

    /**
     * @SerializedName("@POSTALCODE");
     * @example 453259
     * @var string
     */
    protected $postalCode;

    /**
     * @SerializedName("@IFNSFL");
     * @example 0261
     * @var string
     */
    protected $ifnsFl;

    /**
     * @SerializedName("@IFNSUL");
     * @example 0261
     * @var string
     */
    protected $ifnsUl;

    /**
     * @SerializedName("@TERRIFNSFL");
     * @example 0266
     * @var string
     */
    protected $terrIfnsFl;

    /**
     * @SerializedName("@TERRIFNSUL");
     * @example 0266
     * @var string
     */
    protected $terrIfnsUl;

    /**
     * @SerializedName("@OKATO");
     * @example 80439000000
     * @var string
     */
    protected $okato;

    /**
     * @SerializedName("@OKTMO");
     * @example 80739000001
     * @var string
     */
    protected $oktmo;

    /**
     * @SerializedName("@PARENTGUID");
     * @example 12a34da7-983f-4b54-99a6-8f1083d89487
     * @var string
     */
    protected $parentGuid;

    /**
     * @SerializedName("@STEADID");
     * @example ff3a08df-3280-434d-99a1-f7246912c813
     * @var string
     */
    protected $id;

    /**
     * @SerializedName("@PREVID");
     * @example f87eff81-12b2-4946-b648-f33a6d5d9c46
     * @var string
     */
    protected $prevId;

    /**
     * @SerializedName("@OPERSTATUS");
     * @example 10
     * @var string
     */
    protected $operStatus;

    /**
     * @SerializedName("@LIVESTATUS");
     * @example 1
     * @var string
     */
    protected $liveStatus;

    /**
     * @SerializedName("@DIVTYPE");
     * @example 1
     * @var string
     */
    protected $divType;

    /**
     * @SerializedName("@UPDATEDATE")
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @SerializedName("@STARTDATE")
     * @example 2020-03-05
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @SerializedName("@ENDDATE")
     * @example 9999-12-31
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid(string $guid): void
    {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getRegionCode(): string
    {
        return $this->regionCode;
    }

    /**
     * @param string $regionCode
     */
    public function setRegionCode(string $regionCode): void
    {
        $this->regionCode = $regionCode;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getIfnsFl(): string
    {
        return $this->ifnsFl;
    }

    /**
     * @param string $ifnsFl
     */
    public function setIfnsFl(string $ifnsFl): void
    {
        $this->ifnsFl = $ifnsFl;
    }

    /**
     * @return string
     */
    public function getIfnsUl(): string
    {
        return $this->ifnsUl;
    }

    /**
     * @param string $ifnsUl
     */
    public function setIfnsUl(string $ifnsUl): void
    {
        $this->ifnsUl = $ifnsUl;
    }

    /**
     * @return string
     */
    public function getTerrIfnsFl(): string
    {
        return $this->terrIfnsFl;
    }

    /**
     * @param string $terrIfnsFl
     */
    public function setTerrIfnsFl(string $terrIfnsFl): void
    {
        $this->terrIfnsFl = $terrIfnsFl;
    }

    /**
     * @return string
     */
    public function getTerrIfnsUl(): string
    {
        return $this->terrIfnsUl;
    }

    /**
     * @param string $terrIfnsUl
     */
    public function setTerrIfnsUl(string $terrIfnsUl): void
    {
        $this->terrIfnsUl = $terrIfnsUl;
    }

    /**
     * @return string
     */
    public function getOkato(): string
    {
        return $this->okato;
    }

    /**
     * @param string $okato
     */
    public function setOkato(string $okato): void
    {
        $this->okato = $okato;
    }

    /**
     * @return string
     */
    public function getOktmo(): string
    {
        return $this->oktmo;
    }

    /**
     * @param string $oktmo
     */
    public function setOktmo(string $oktmo): void
    {
        $this->oktmo = $oktmo;
    }

    /**
     * @return string
     */
    public function getParentGuid(): string
    {
        return $this->parentGuid;
    }

    /**
     * @param string $parentGuid
     */
    public function setParentGuid(string $parentGuid): void
    {
        $this->parentGuid = $parentGuid;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPrevId(): string
    {
        return $this->prevId;
    }

    /**
     * @param string $prevId
     */
    public function setPrevId(string $prevId): void
    {
        $this->prevId = $prevId;
    }

    /**
     * @return string
     */
    public function getOperStatus(): string
    {
        return $this->operStatus;
    }

    /**
     * @param string $operStatus
     */
    public function setOperStatus(string $operStatus): void
    {
        $this->operStatus = $operStatus;
    }

    /**
     * @return string
     */
    public function getLiveStatus(): string
    {
        return $this->liveStatus;
    }

    /**
     * @param string $liveStatus
     */
    public function setLiveStatus(string $liveStatus): void
    {
        $this->liveStatus = $liveStatus;
    }

    /**
     * @return string
     */
    public function getDivType(): string
    {
        return $this->divType;
    }

    /**
     * @param string $divType
     */
    public function setDivType(string $divType): void
    {
        $this->divType = $divType;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate(): \DateTime
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime $updateDate
     */
    public function setUpdateDate(\DateTime $updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate(\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function setEndDate(\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }
}
