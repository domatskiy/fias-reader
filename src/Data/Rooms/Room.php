<?php

namespace Domatskiy\FiasReader\Data\Rooms;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Room
{
    /**
     * @SerializedName("@ROOMID")
     * @var string
     */
    protected $id;

    /**
     * @SerializedName("@ROOMGUID")
     * @var string
     */
    protected $guid;

    /**
     * @SerializedName("@HOUSEGUID")
     * @var string
     */
    protected $houseGuid;

    /**
     * @SerializedName("@REGIONCODE")
     * @var string
     */
    protected $regionCode;

    /**
     * @SerializedName("@FLATNUMBER")
     * @var string
     */
    protected $flatNumber;

    /**
     * @SerializedName("@FLATTYPE")
     * @var string
     */
    protected $flatType;

    /**
     * @SerializedName("@POSTALCODE")
     * @var string
     */
    protected $postalCode;

    /**
     * @SerializedName("@PREVID")
     * @var string
     */
    protected $prevId;

    /**
     * @SerializedName("@OPERSTATUS")
     * @var string
     */
    protected $operStatus;

    /**
     * @SerializedName("@UPDATEDATE")
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @SerializedName("@STARTDATE")
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @SerializedName("@ENDDATE")
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @SerializedName("@LIVESTATUS")
     * @var string
     */
    protected $liveStatus;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid(string $guid): void
    {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function getHouseGuid(): string
    {
        return $this->houseGuid;
    }

    /**
     * @param string $houseGuid
     */
    public function setHouseGuid(string $houseGuid): void
    {
        $this->houseGuid = $houseGuid;
    }

    /**
     * @return string
     */
    public function getRegionCode(): string
    {
        return $this->regionCode;
    }

    /**
     * @param string $regionCode
     */
    public function setRegionCode(string $regionCode): void
    {
        $this->regionCode = $regionCode;
    }

    /**
     * @return string
     */
    public function getFlatNumber(): string
    {
        return $this->flatNumber;
    }

    /**
     * @param string $flatNumber
     */
    public function setFlatNumber(string $flatNumber): void
    {
        $this->flatNumber = $flatNumber;
    }

    /**
     * @return string
     */
    public function getFlatType(): string
    {
        return $this->flatType;
    }

    /**
     * @param string $flatType
     */
    public function setFlatType(string $flatType): void
    {
        $this->flatType = $flatType;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getPrevId(): string
    {
        return $this->prevId;
    }

    /**
     * @param string $prevId
     */
    public function setPrevId(string $prevId): void
    {
        $this->prevId = $prevId;
    }

    /**
     * @return string
     */
    public function getOperStatus(): string
    {
        return $this->operStatus;
    }

    /**
     * @param string $operStatus
     */
    public function setOperStatus(string $operStatus): void
    {
        $this->operStatus = $operStatus;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate(): \DateTime
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime $updateDate
     */
    public function setUpdateDate(\DateTime $updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate(\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function setEndDate(\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getLiveStatus(): string
    {
        return $this->liveStatus;
    }

    /**
     * @param string $liveStatus
     */
    public function setLiveStatus(string $liveStatus): void
    {
        $this->liveStatus = $liveStatus;
    }
}
