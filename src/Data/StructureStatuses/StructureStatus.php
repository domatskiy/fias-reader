<?php

namespace Domatskiy\FiasReader\Data\StructureStatuses;

use Symfony\Component\Serializer\Annotation\SerializedName;

class StructureStatus
{
    /**
     * @SerializedName("@STRSTATID")
     * @var string
     */
    protected $id;

    /**
     * @SerializedName("@NAME")
     * @var string
     */
    protected $name;

    /**
     * @SerializedName("@SHORTNAME")
     * @var string
     */
    protected $shortName;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     */
    public function setShortName(string $shortName): void
    {
        $this->shortName = $shortName;
    }
}
