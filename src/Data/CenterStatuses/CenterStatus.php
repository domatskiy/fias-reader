<?php

namespace Domatskiy\FiasReader\Data\CenterStatuses;

use Symfony\Component\Serializer\Annotation\SerializedName;

class CenterStatus
{
    /**
     * @SerializedName("@CENTERSTID")
     * @var string
     */
    protected $id;

    /**
     * @SerializedName("@NAME")
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
