<?php

namespace Domatskiy\FiasReader\Data\AddressObjectTypes;

use Symfony\Component\Serializer\Annotation\SerializedName;

class AddressObjectType
{
    /**
     * @SerializedName("@LEVEL")
     * @var string
     */
    protected $level;

    /**
     * @SerializedName("@SOCRNAME")
     * @var string
     */
    protected $socrName;

    /**
     * @SerializedName("@SCNAME")
     * @example Автономный округ
     * @var string
     */
    protected $scName;

    /**
     * @SerializedName("@KOD_T_ST")
     * @example 109
     * @var string
     */
    protected $kodTSt;

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level): void
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getSocrName(): string
    {
        return $this->socrName;
    }

    /**
     * @param string $socrName
     */
    public function setSocrName(string $socrName): void
    {
        $this->socrName = $socrName;
    }

    /**
     * @return string
     */
    public function getScName(): string
    {
        return $this->scName;
    }

    /**
     * @param string $scName
     */
    public function setScName(string $scName): void
    {
        $this->scName = $scName;
    }

    /**
     * @return string
     */
    public function getKodTSt(): string
    {
        return $this->kodTSt;
    }

    /**
     * @param string $kodTSt
     */
    public function setKodTSt(string $kodTSt): void
    {
        $this->kodTSt = $kodTSt;
    }
}
