<?php

namespace Domatskiy\FiasReader\Data\NormativeDocumentes;

use Symfony\Component\Serializer\Annotation\SerializedName;

class NormativeDocument
{
    /**
     * @SerializedName("@NORMDOCID")
     * @example c85608cc-8d77-416a-bdcc-dacba1d98fdd
     * @var string
     */
    protected $normDocId;

    /**
     * @SerializedName("@DOCNAME")
     * @example Об утверждении ведомственного классификатора...
     * @var string
     */
    protected $docName;

    /**
     * @SerializedName("@DOCDATE")
     * @example 22.01.01 0:00:00
     * @var string
     */
    protected $docDate;

    /**
     * @SerializedName("@DOCNUM")
     * @example БГ-3-14/17
     * @var string
     */
    protected $docNum;

    /**
     * @SerializedName("@DOCTYPE")
     * @example 24
     * @var string
     */
    protected $docType;

    /**
     * @SerializedName("@DOCIMGID")
     * @example 048a0d2e-013f-4ee8-a768-d44d78bd13f9
     * @var string
     *///
    protected $docImgId;

    /**
     * @return string
     */
    public function getNormDocId(): string
    {
        return $this->normDocId;
    }

    /**
     * @param string $normDocId
     */
    public function setNormDocId(string $normDocId): void
    {
        $this->normDocId = $normDocId;
    }

    /**
     * @return string
     */
    public function getDocName(): string
    {
        return $this->docName;
    }

    /**
     * @param string $docName
     */
    public function setDocName(string $docName): void
    {
        $this->docName = $docName;
    }

    /**
     * @return string
     */
    public function getDocDate(): string
    {
        return $this->docDate;
    }

    /**
     * @param string $docDate
     */
    public function setDocDate(string $docDate): void
    {
        $this->docDate = $docDate;
    }

    /**
     * @return string
     */
    public function getDocNum(): string
    {
        return $this->docNum;
    }

    /**
     * @param string $docNum
     */
    public function setDocNum(string $docNum): void
    {
        $this->docNum = $docNum;
    }

    /**
     * @return string
     */
    public function getDocType(): string
    {
        return $this->docType;
    }

    /**
     * @param string $docType
     */
    public function setDocType(string $docType): void
    {
        $this->docType = $docType;
    }

    /**
     * @return string
     */
    public function getDocImgId(): string
    {
        return $this->docImgId;
    }

    /**
     * @param string $docImgId
     */
    public function setDocImgId(string $docImgId): void
    {
        $this->docImgId = $docImgId;
    }
}
