<?php

namespace Domatskiy\FiasReader\Data\Houses;

use Symfony\Component\Serializer\Annotation\SerializedName;

class House
{
    /**
     * @SerializedName("@HOUSEID")
     * @example 9e6dccf6-bf21-4a77-b4dd-056b26159518
     * @var string
     */
    protected $id;

    /**
     * @SerializedName("@HOUSEGUID")
     * @example a5723a10-0a0e-47db-99e8-dab94dcee2ae
     * @var string
     */
    protected $guid;

    /**
     * @SerializedName("@HOUSENUM");
     * @var string
     */
    protected $houseNumber;

    /**
     * @SerializedName("@REGIONCODE");
     * @var string
     */
    protected $regionCode;

    /**
     * @SerializedName("@POSTALCODE");
     * @example 453259
     * @var string
     */
    protected $postalCode;

    /**
     * @SerializedName("@AOGUID")
     * @example 77d25ec7-8ca5-4373-82f0-95d195bc1a78
     * @var string|null
     */
    protected $AoGuid;

    /**
     * @SerializedName("@STRUCNUM")
     * @example 47
     * @var string|null
     */
    protected $strucNum;

    /**
     * @SerializedName("@STRSTATUS")
     * @example 1
     * @var string|null
     */
    protected $strStatus;

    /**
     * @SerializedName("@ESTSTATUS")
     * @example 2
     * @var string|null
     */
    protected $estStatus;

    /**
     * @SerializedName("@STATSTATUS")
     * @example 0
     * @var string|null
     */
    protected $statStatus;

    /**
     * @SerializedName("@CADNUM")
     * @example 02:73:010711:516
     * @var string|null
     */
    protected $cadNum;

    /**
     * @SerializedName("@IFNSFL");
     * @example 0261
     * @var string
     */
    protected $ifnsFl;

    /**
     * @SerializedName("@IFNSUL");
     * @example 0261
     * @var string
     */
    protected $ifnsUl;

    /**
     * @SerializedName("@TERRIFNSFL");
     * @example 0266
     * @var string
     */
    protected $terrIfnsFl;

    /**
     * @SerializedName("@TERRIFNSUL");
     * @example 0266
     * @var string
     */
    protected $terrIfnsUl;

    /**
     * @SerializedName("@OKATO");
     * @example 80439000000
     * @var string
     */
    protected $okato;

    /**
     * @SerializedName("@OKTMO");
     * @example 80739000001
     * @var string
     */
    protected $oktmo;

    /**
     * @SerializedName("@UPDATEDATE")
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @SerializedName("@STARTDATE")
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @SerializedName("@ENDDATE")
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @SerializedName("@DIVTYPE");
     * @example 1
     * @var string
     */
    protected $divType;

    /**
     * @SerializedName("@COUNTER");
     * @example 38
     * @var string
     */
    protected $counter;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid(string $guid): void
    {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber(string $houseNumber): void
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getRegionCode(): string
    {
        return $this->regionCode;
    }

    /**
     * @param string $regionCode
     */
    public function setRegionCode(string $regionCode): void
    {
        $this->regionCode = $regionCode;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string|null
     */
    public function getAoGuid(): ?string
    {
        return $this->AoGuid;
    }

    /**
     * @param string|null $AoGuid
     */
    public function setAoGuid(?string $AoGuid): void
    {
        $this->AoGuid = $AoGuid;
    }

    /**
     * @return string|null
     */
    public function getStrucNum(): ?string
    {
        return $this->strucNum;
    }

    /**
     * @param string|null $strucNum
     */
    public function setStrucNum(?string $strucNum): void
    {
        $this->strucNum = $strucNum;
    }

    /**
     * @return string|null
     */
    public function getStrStatus(): ?string
    {
        return $this->strStatus;
    }

    /**
     * @param string|null $strStatus
     */
    public function setStrStatus(?string $strStatus): void
    {
        $this->strStatus = $strStatus;
    }

    /**
     * @return string|null
     */
    public function getEstStatus(): ?string
    {
        return $this->estStatus;
    }

    /**
     * @param string|null $estStatus
     */
    public function setEstStatus(?string $estStatus): void
    {
        $this->estStatus = $estStatus;
    }

    /**
     * @return string|null
     */
    public function getStatStatus(): ?string
    {
        return $this->statStatus;
    }

    /**
     * @param string|null $statStatus
     */
    public function setStatStatus(?string $statStatus): void
    {
        $this->statStatus = $statStatus;
    }

    /**
     * @return string|null
     */
    public function getCadNum(): ?string
    {
        return $this->cadNum;
    }

    /**
     * @param string|null $cadNum
     */
    public function setCadNum(?string $cadNum): void
    {
        $this->cadNum = $cadNum;
    }

    /**
     * @return string
     */
    public function getIfnsFl(): string
    {
        return $this->ifnsFl;
    }

    /**
     * @param string $ifnsFl
     */
    public function setIfnsFl(string $ifnsFl): void
    {
        $this->ifnsFl = $ifnsFl;
    }

    /**
     * @return string
     */
    public function getIfnsUl(): string
    {
        return $this->ifnsUl;
    }

    /**
     * @param string $ifnsUl
     */
    public function setIfnsUl(string $ifnsUl): void
    {
        $this->ifnsUl = $ifnsUl;
    }

    /**
     * @return string
     */
    public function getTerrIfnsFl(): string
    {
        return $this->terrIfnsFl;
    }

    /**
     * @param string $terrIfnsFl
     */
    public function setTerrIfnsFl(string $terrIfnsFl): void
    {
        $this->terrIfnsFl = $terrIfnsFl;
    }

    /**
     * @return string
     */
    public function getTerrIfnsUl(): string
    {
        return $this->terrIfnsUl;
    }

    /**
     * @param string $terrIfnsUl
     */
    public function setTerrIfnsUl(string $terrIfnsUl): void
    {
        $this->terrIfnsUl = $terrIfnsUl;
    }

    /**
     * @return string
     */
    public function getOkato(): string
    {
        return $this->okato;
    }

    /**
     * @param string $okato
     */
    public function setOkato(string $okato): void
    {
        $this->okato = $okato;
    }

    /**
     * @return string
     */
    public function getOktmo(): string
    {
        return $this->oktmo;
    }

    /**
     * @param string $oktmo
     */
    public function setOktmo(string $oktmo): void
    {
        $this->oktmo = $oktmo;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate(): \DateTime
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime $updateDate
     */
    public function setUpdateDate(\DateTime $updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate(\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function setEndDate(\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getDivType(): string
    {
        return $this->divType;
    }

    /**
     * @param string $divType
     */
    public function setDivType(string $divType): void
    {
        $this->divType = $divType;
    }

    /**
     * @return string
     */
    public function getCounter(): string
    {
        return $this->counter;
    }

    /**
     * @param string $counter
     */
    public function setCounter(string $counter): void
    {
        $this->counter = $counter;
    }
}
