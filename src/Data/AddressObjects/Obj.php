<?php

namespace Domatskiy\FiasReader\Data\AddressObjects;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Obj
{
    /**
     * Уникальный идентификатор записи. Ключевое поле.
     * @SerializedName("@AOID")
     * @var string|null
     */
    protected $AoID;

    /**
     * Глобальный уникальный идентификатор адресного объекта
     * @SerializedName("@AOGUID")
     * @var string|null
     */
    protected $AoGuid;

    /**
     * Идентификатор объекта родительского объекта
     * @SerializedName("@PARENTGUID")
     * @var string|null
     */
    protected $parentGuid;

    /**
     * Идентификатор записи связывания с предыдушей исторической записью
     * @SerializedName("@PREVID")
     * @var string|null
     */
    protected $prevId;

    /**
     * Формализованное наименование
     * @SerializedName("@FORMALNAME")
     * @var string|null
     */
    protected $formalName;

    /**
     * Официальное наименование
     * @SerializedName("@OFFNAME")
     * @var string|null
     */
    protected $offName;

    /**
     * Краткое наименование типа объекта
     * @SerializedName("@SHORTNAME")
     * @var string|null
     */
    protected $shortName;

    /**
     * Уровень адресного объекта
     * @SerializedName("@AOLEVEL")
     * @var string|int|null
     */
    protected $AoLevel;

    /**
     * Код региона
     * @SerializedName("@REGIONCODE")
     * @var string|null
     */
    protected $regionCode;

    /**
     * Код автономии
     * @SerializedName("@AUTOCODE")
     * @var string|null
     */
    protected $autoCode;

    /**
     * Код района
     * @SerializedName("@AREACODE")
     * @var string|null
     */
    protected $areaCode;

    /**
     * Код города
     * @SerializedName("@CITYCODE")
     * @var string|null
     */
    protected $cityCode;


    /**
     * Код внутригородского района
     * @SerializedName("@CTARCODE")
     * @var string|null
     */
    protected $CtarCode;

    /**
     * Код населенного пункта
     * @SerializedName("@PLACECODE")
     * @var string|null
     */
    protected $placeCode;

    /**
     * Код элемента планировочной структуры
     * @SerializedName("@PLANCODE")
     * @var string|null
     */
    protected $planCode;

    /**
     * Код улицы
     * @SerializedName("@STREETCODE")
     * @var string|null
     */
    protected $streetCode;

    /**
     * Код дополнительного адресообразующего элемента
     * @SerializedName("@EXTRCODE")
     * @var string|null
     */
    protected $extrCode;

    /**
     * Код подчиненного дополнительного адресообразующего элемента
     * @SerializedName("@SEXTCODE")
     * @example 000
     * @var string|null
     */
    protected $sextCode;

    /**
     * Код адресного элемента одной строкой без признака актуальности (последних двух цифр)
     * @SerializedName("@PLAINCODE")
     * @example 020000060000431
     * @var string|null
     */
    protected $plainCode;

    /**
     * Код адресного элемента одной строкой с признаком актуальности из классификационного кода
     * @SerializedName("@CODE")
     * @example 02000006000043100
     * @var string|null
     */
    protected $code;

    /**
     * Статус актуальности КЛАДР 4 (последние две цифры в коде)
     * @SerializedName("@CURRSTATUS")
     * @example 0
     * @var string|null
     */
    protected $currStatus;

    /**
     * Статус последней исторической записи в жизненном цикле адресного объекта: 0 – Не последняя 1 - Последняя
     * @SerializedName("@ACTSTATUS")
     * @example 1
     * @var string|null
     */
    protected $actStatus;

    /**
     * Статус актуальности адресного объекта ФИАС на текущую дату: 0 – Не актуальный 1 - Актуальный
     * @SerializedName("@LIVESTATUS")
     * @example 1
     * @var string|null
     */
    protected $liveStatus;

    /**
     * Статус центра
     * @SerializedName("@CENTSTATUS")
     * @example 0
     * @var string|int|null
     */
    protected $centStatus;

    /**
     * Статус действия над записью – причина появления записи (см. OperationStatuses )
     * @SerializedName("@OPERSTATUS")
     * @example 21
     * @var string|null
     */
    protected $operStatus;

    /**
     * Код ИФНС ФЛ
     * @SerializedName("@IFNSFL")
     * @example 0267
     * @var string|null
     */
    protected $ifnsFl;

    /**
     * Код ИФНС ЮЛ
     * @SerializedName("@IFNSUL")
     * @example 0267
     * @var string|null
     */
    protected $ifnsUl;

    /**
     * @SerializedName("@OKATO")
     * @example 80443000000
     * @var string|null
     */
    protected $okato;

    /**
     * @SerializedName("@OKTMO")
     * @example 80743000001
     * @var string|null
     */
    protected $oktmo;

    /**
     * Почтовый индекс
     * @SerializedName("@POSTALCODE")
     * @example 453830
     * @var string|null
     */
    protected $postalCode;

    /**
     * Начало действия записи
     * @SerializedName("@STARTDATE")
     * @example 2020-03-05
     * @var \DateTime|null
     */
    protected $startDate;

    /**
     * Дата  внесения (обновления) записи
     * @SerializedName("@UPDATEDATE")
     * @example 2020-03-05
     * @var \DateTime|null
     */
    protected $updateDate;

    /**
     * Окончание действия записи
     * @SerializedName("@ENDDATE")
     * @example 9999-12-31
     * @var \DateTime|null
     */
    protected $endDate;

    /**
     * Тип деления: 0 – не определено 1 – муниципальное 2 – административное
     * @SerializedName("@DIVTYPE")
     * @example 0
     * @var string|null
     */
    protected $divType;

    /**
     * @return string|null
     */
    public function getAoID(): ?string
    {
        return $this->AoID;
    }

    /**
     * @param string|null $AoID
     */
    public function setAoID(?string $AoID): void
    {
        $this->AoID = $AoID;
    }

    /**
     * @return string|null
     */
    public function getAoGuid(): ?string
    {
        return $this->AoGuid;
    }

    /**
     * @param string|null $AoGuid
     */
    public function setAoGuid(?string $AoGuid): void
    {
        $this->AoGuid = $AoGuid;
    }

    /**
     * Идентификатор объекта родительского объекта
     * @return string|null
     */
    public function getParentGuid(): ?string
    {
        return $this->parentGuid;
    }

    /**
     * @param string|null $parentGuid
     */
    public function setParentGuid(?string $parentGuid): void
    {
        $this->parentGuid = $parentGuid;
    }

    /**
     * @return string|null
     */
    public function getPrevId(): ?string
    {
        return $this->prevId;
    }

    /**
     * @param string|null $prevId
     */
    public function setPrevId(?string $prevId): void
    {
        $this->prevId = $prevId;
    }

    /**
     * @return string|null
     */
    public function getFormalName(): ?string
    {
        return $this->formalName;
    }

    /**
     * @param string|null $formalName
     */
    public function setFormalName(?string $formalName): void
    {
        $this->formalName = $formalName;
    }

    /**
     * Официальное наименование
     * @return string|null
     */
    public function getOffName(): ?string
    {
        return $this->offName;
    }

    /**
     * @param string|null $offName
     */
    public function setOffName(?string $offName): void
    {
        $this->offName = $offName;
    }

    /**
     * Краткое наименование типа объекта
     * @return string|null
     */
    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    /**
     * @param string|null $shortName
     */
    public function setShortName(?string $shortName): void
    {
        $this->shortName = $shortName;
    }

    /**
     * Уровень адресного объекта
     * @return string|null
     */
    public function getAoLevel(): ?string
    {
        return $this->AoLevel;
    }

    /**
     * @param string|null $AoLevel
     */
    public function setAoLevel(?string $AoLevel): void
    {
        $this->AoLevel = $AoLevel;
    }

    /**
     * @return string|null
     */
    public function getRegionCode(): ?string
    {
        return $this->regionCode;
    }

    /**
     * @param string|null $regionCode
     */
    public function setRegionCode(?string $regionCode): void
    {
        $this->regionCode = $regionCode;
    }

    /**
     * @return string|null
     */
    public function getAutoCode(): ?string
    {
        return $this->autoCode;
    }

    /**
     * @param string|null $autoCode
     */
    public function setAutoCode(?string $autoCode): void
    {
        $this->autoCode = $autoCode;
    }

    /**
     * @return string|null
     */
    public function getCityCode(): ?string
    {
        return $this->cityCode;
    }

    /**
     * @param string|null $cityCode
     */
    public function setCityCode(?string $cityCode): void
    {
        $this->cityCode = $cityCode;
    }

    /**
     * @return string|null
     */
    public function getCtarCode(): ?string
    {
        return $this->CtarCode;
    }

    /**
     * @param string|null $CtarCode
     */
    public function setCtarCode(?string $CtarCode): void
    {
        $this->CtarCode = $CtarCode;
    }

    /**
     * @return string|null
     */
    public function getPlaceCode(): ?string
    {
        return $this->placeCode;
    }

    /**
     * @param string|null $placeCode
     */
    public function setPlaceCode(?string $placeCode): void
    {
        $this->placeCode = $placeCode;
    }

    /**
     * @return string|null
     */
    public function getPlanCode(): ?string
    {
        return $this->planCode;
    }

    /**
     * @param string|null $planCode
     */
    public function setPlanCode(?string $planCode): void
    {
        $this->planCode = $planCode;
    }

    /**
     * @return string|null
     */
    public function getStreetCode(): ?string
    {
        return $this->streetCode;
    }

    /**
     * @param string|null $streetCode
     */
    public function setStreetCode(?string $streetCode): void
    {
        $this->streetCode = $streetCode;
    }

    /**
     * @return string|null
     */
    public function getExtrCode(): ?string
    {
        return $this->extrCode;
    }

    /**
     * @param string|null $extrCode
     */
    public function setExtrCode(?string $extrCode): void
    {
        $this->extrCode = $extrCode;
    }

    /**
     * @return string|null
     */
    public function getSextCode(): ?string
    {
        return $this->sextCode;
    }

    /**
     * @param string|null $sextCode
     */
    public function setSextCode(?string $sextCode): void
    {
        $this->sextCode = $sextCode;
    }

    /**
     * Код адресного элемента одной строкой без признака актуальности (последних двух цифр)
     * @return string|null
     */
    public function getPlainCode(): ?string
    {
        return $this->plainCode;
    }

    /**
     * @param string|null $plainCode
     */
    public function setPlainCode(?string $plainCode): void
    {
        $this->plainCode = $plainCode;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * Статус актуальности КЛАДР 4 (последние две цифры в коде)
     * @return string|null
     */
    public function getCurrStatus(): ?string
    {
        return $this->currStatus;
    }

    /**
     * @param string|null $currStatus
     */
    public function setCurrStatus(?string $currStatus): void
    {
        $this->currStatus = $currStatus;
    }

    /**
     * Статус последней исторической записи в жизненном цикле адресного объекта: 0 – Не последняя 1 - Последняя
     * @return string|null
     */
    public function getActStatus(): ?string
    {
        return $this->actStatus;
    }

    /**
     * @param string|null $actStatus
     */
    public function setActStatus(?string $actStatus): void
    {
        $this->actStatus = $actStatus;
    }

    /**
     * Статус актуальности адресного объекта ФИАС на текущую дату: 0 – Не актуальный 1 - Актуальный
     * @return string|null
     */
    public function getLiveStatus(): ?string
    {
        return $this->liveStatus;
    }

    /**
     * Статус актуальности адресного объекта ФИАС на текущую дату: 0 – Не актуальный 1 - Актуальный
     * @param string|null $liveStatus
     */
    public function setLiveStatus(?string $liveStatus): void
    {
        $this->liveStatus = $liveStatus;
    }

    /**
     * Статус центра
     * @return string|null
     */
    public function getCentStatus(): ?string
    {
        return $this->centStatus;
    }

    /**
     * @param string|null $centStatus
     */
    public function setCentStatus(?string $centStatus): void
    {
        $this->centStatus = $centStatus;
    }

    /**
     * Статус действия над записью – причина появления записи (см. OperationStatuses )
     * @return string|null
     */
    public function getOperStatus(): ?string
    {
        return $this->operStatus;
    }

    /**
     * @param string|null $operStatus
     */
    public function setOperStatus(?string $operStatus): void
    {
        $this->operStatus = $operStatus;
    }

    /**
     * @return string|null
     */
    public function getIfnsFl(): ?string
    {
        return $this->ifnsFl;
    }

    /**
     * @param string|null $ifnsFl
     */
    public function setIfnsFl(?int $ifnsFl): void
    {
        $this->ifnsFl = $ifnsFl;
    }

    /**
     * @return string|null
     */
    public function getIfnsUl(): ?string
    {
        return $this->ifnsUl;
    }

    /**
     * @param string|null $ifnsUl
     */
    public function setIfnsUl(?string $ifnsUl): void
    {
        $this->ifnsUl = $ifnsUl;
    }

    /**
     * @return string|null
     */
    public function getOkato(): ?string
    {
        return $this->okato;
    }

    /**
     * @param string|null $okato
     */
    public function setOkato(?string $okato): void
    {
        $this->okato = $okato;
    }

    /**
     * @return string|null
     */
    public function getOktmo(): ?string
    {
        return $this->oktmo;
    }

    /**
     * @param string|null $oktmo
     */
    public function setOktmo(?string $oktmo): void
    {
        $this->oktmo = $oktmo;
    }

    /**
     * Почтовый индекс
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     */
    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * Начало действия записи
     * @return \DateTime|null
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime|null $startDate
     */
    public function setStartDate(?\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * Дата  внесения (обновления) записи
     * @return \DateTime|null
     */
    public function getUpdateDate(): ?\DateTime
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime|null $updateDate
     */
    public function setUpdateDate(?\DateTime $updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * Окончание действия записи
     * @return \DateTime|null
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime|null $endDate
     */
    public function setEndDate(?\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * Тип деления: 0 – не определено 1 – муниципальное 2 – административное
     * @return string|null
     */
    public function getDivType(): ?string
    {
        return $this->divType;
    }

    /**
     * @param string|null $divType
     */
    public function setDivType(?string $divType): void
    {
        $this->divType = $divType;
    }
}
